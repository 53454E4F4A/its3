package A3;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


public class RSAKeyCreation {
	
	private static final String usage = "java A3.RSAKeyCreation <name>";

	public static void main(String[] args) {
    try {    	
    	String name = args[0];
      
    	// Generate Keypair
    	KeyPairGenerator  kg = KeyPairGenerator.getInstance("RSA");
        kg.initialize(2048); 
        KeyPair keyPair = kg.genKeyPair();
        
        // Encode public Key
        byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
        X509EncodedKeySpec x509ks = new X509EncodedKeySpec(
        		publicKeyBytes);        
        byte[] encodedPublicKeyBytes = x509ks.getEncoded();     
        

        // write to File
        DataOutputStream os = new DataOutputStream(new FileOutputStream(
                name + ".pub"));
        os.writeInt(name.length());
        os.writeChars(name);
        os.writeInt(encodedPublicKeyBytes.length);
        os.write(encodedPublicKeyBytes);  
        os.close();              
      
       
        // Encode private Key
        byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();    
        PKCS8EncodedKeySpec pkcsKeySpec = new PKCS8EncodedKeySpec(
        		privateKeyBytes);       
        byte[] encodedPrivateKeyBytes = pkcsKeySpec.getEncoded();  
       
        // write to File
        os = new DataOutputStream(new FileOutputStream(
                name + ".prv"));
        os.writeInt(name.length());
        os.writeChars(name);
        os.writeInt(encodedPrivateKeyBytes.length);
        os.write(encodedPrivateKeyBytes);  
        os.close();            
         

    } catch (ArrayIndexOutOfBoundsException e) {
    	System.out.println(usage);
    } catch (Exception ex) {
        // ein Fehler???
        System.out.println("Error: " + ex.getMessage());
    }
}

}
