package A3;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class SendSecureFile {
	@SuppressWarnings("unused")
	private String name;

	private PKCS8EncodedKeySpec privateKey;
	private X509EncodedKeySpec publicKey;

	private byte[] encryptedAESKey = null;
	private SecretKey aesKey = null;
	private byte[] signature = null;

	private byte[] encryptedFile = null;
	private final static String usage = "java A3.SendSecureFile <prv> <prb> <input file> <output file>";

	public static void main(String[] args) {
		try {
		SendSecureFile SSF = new SendSecureFile();
		SSF.readKeyFile(args[0], false);
		SSF.readKeyFile(args[1], true);
		SSF.createAESKeyAndSignature();
		SSF.wrapKey();
		String checksum = "MD5";
		if(args.length > 4) checksum = args[4];
		SSF.encryptFile(args[2], checksum);
		SSF.createOutputFile(args[3]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(usage);
		}
	}
	/* Creates output file (g)*/
	void createOutputFile(String file) {
		
        try {
		 DataOutputStream os = new DataOutputStream(new FileOutputStream(file));
	     os.writeInt(encryptedAESKey.length);
	     os.write(encryptedAESKey);
	     os.writeInt(signature.length);
	     os.write(signature); 
	     os.write(encryptedFile);
	     os.close();       
        
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/* encrypts file with AES and calculates checksums (f)*/
	void encryptFile(String in, String hashFunction) {
		
		
		try {
			int len;
			
			// Create Input and Output streams;
		    File fileIn = new File(in);		    
		    FileInputStream fileInStream = new FileInputStream(in);		      
		    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		    
		    // Create Digest
		    MessageDigest md = MessageDigest.getInstance(hashFunction);
			DigestInputStream dis = new DigestInputStream(fileInStream, md);
			
			
			// Read File
		    byte[] fileBytes = new byte[(int) fileIn.length()];		  
		    
		    while((len = fileInStream.read(fileBytes)) > 0) {
		    
		    	byteArrayOutputStream.write(fileBytes, 0, len);
			}
		    dis.close();
		    fileInStream.close();
		    
		    fileBytes = byteArrayOutputStream.toByteArray();
		    
		    // unencrypted checksum
		    byte[] checksum = md.digest();
		    StringBuilder sb = new StringBuilder();
		    for (byte b : checksum) {
		        sb.append(String.format("%02x", b));
		    }
		    
		    System.out.println("unencrypted: \n\tchecksum:\t" + sb.toString() + " (" + md.getAlgorithm() +  ")");		    
		    System.out.println("\tFile size:\t" + fileBytes.length + " Byte");

		    // encrypt file
		    
		    Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);			
		
			byteArrayOutputStream = new ByteArrayOutputStream();
		    CipherOutputStream cos = new CipherOutputStream(byteArrayOutputStream, cipher);	   
		    
		    cos.write(fileBytes);	  
		    cos.close();
		    
		    encryptedFile = byteArrayOutputStream.toByteArray();
		    
		    // encrypted checksum
		    checksum = md.digest(encryptedFile);
		    sb = new StringBuilder();
		    for (byte b : checksum) {
		        sb.append(String.format("%02x", b));
		    }
		    
		    System.out.println("encrypted: \n\tchecksum\t" + sb.toString() + " (" + md.getAlgorithm() +  ")");
		    System.out.println("\tFile size:\t" + encryptedFile.length + " Byte");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/* Wraps AES key with public RSA key (e) */
	void wrapKey() {
		
		
		try {
		
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey pkey = keyFactory.generatePublic(publicKey);
		
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.WRAP_MODE, pkey);		
		encryptedAESKey = cipher.wrap(aesKey);
		
		} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
	}
	
	/* Creates AES Key and Signature with private RSA key (c & d) */
	void createAESKeyAndSignature() {
		try {

			KeyGenerator kg = KeyGenerator.getInstance("AES");
			kg.init(128); // 
			aesKey = kg.generateKey();

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey pkey = keyFactory.generatePrivate(privateKey);

			Signature rsaSig = Signature.getInstance("SHA1withRSA");
			rsaSig.initSign(pkey);
			rsaSig.update(aesKey.getEncoded());
			signature = rsaSig.sign();
	

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println("keine implementierung");
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/* Reads key File (a & b)*/
	void readKeyFile(String path, boolean isPublic) {
		try {
			
			DataInputStream is = new DataInputStream(new FileInputStream(
	                    path));
	       
	            int len = is.readInt();
	
	            byte[] name = new byte[len*2];	         
	            is.read(name);
	            this.name = String.valueOf(name);
	            len = is.readInt();
	  
	            byte[] key = new byte[len];
	         
	   
	            is.read(key);
	            is.close();
	        
	            if (isPublic) {
	            	
					publicKey = new X509EncodedKeySpec(key);
					
				} else {
					privateKey = new PKCS8EncodedKeySpec(key);
				
				}
	           
			
		} catch (IOException e) {
			System.out.println(e);
		} 

	}

}
