package A3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.DigestOutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;

public class RecieveSecureFile {
	private final static String usage = "java A3.RecieveSecureFile <prv> <pub> <ssf> <output file>";
	String name;
	private X509EncodedKeySpec publicKey;
	private PKCS8EncodedKeySpec privateKey;
	private byte encryptedAESKey[];
	private byte signature[];
	private byte encryptedFile[];
	Key aesKey;

	public static void main(String[] args) {
		RecieveSecureFile RSF = new RecieveSecureFile();
		try {
		RSF.readKeyFile(args[0], false);
		RSF.readKeyFile(args[1], true);
		RSF.readInputFile(args[2]);
		RSF.unwrapKey();
		String checksum = "MD5";
		if(args.length > 4) checksum = args[4];
		RSF.decryptFile(args[3], checksum);
		RSF.verifySignature();
	
		} catch (ArrayIndexOutOfBoundsException e) {
	    	System.out.println(usage);
	    }
		
	}
	/* Verifies the decrypted AES key with the Public RSA key (d) */
	boolean verifySignature() {
		boolean ok = false;
		try {
			KeyFactory keyFac = KeyFactory.getInstance("RSA");
			PublicKey pubKey = keyFac.generatePublic(publicKey);

			Signature rsaSig = Signature.getInstance("SHA1withRSA");

			rsaSig.initVerify(pubKey);

			rsaSig.update(aesKey.getEncoded());


			ok = rsaSig.verify(signature);
			if (ok)
				System.out.println("Signatur erfolgreich verifiziert!");
			else
				System.out.println("Signatur konnte nicht verifiziert werden!");

		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ok;
	}
	/* decrypts the loaded file with the unwrapped AES key */
	void decryptFile(String outfile, String hashFunciton) {
		try {

			Cipher cipher = Cipher.getInstance("AES");

			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
					encryptedFile);
			CipherInputStream cis = new CipherInputStream(byteArrayInputStream,
					cipher);
		
			FileOutputStream fout = new FileOutputStream(outfile);
			int len;
			byte buffer[] = new byte[encryptedFile.length];
		   
		    MessageDigest md = MessageDigest.getInstance(hashFunciton);
		    byte[] checksum = md.digest(encryptedFile);
		    StringBuilder sb = new StringBuilder();
		    for (byte b : checksum) {
		        sb.append(String.format("%02x", b));
		    }
		    System.out.println("encrypted: \n\tchecksum:\t" + sb.toString() + " (" + md.getAlgorithm() +  ")"+"\n\tFile size:\t" + encryptedFile.length + " Byte");
	    
		    DigestOutputStream dos = new DigestOutputStream(fout, md);
		    int x = 0;
			while ((len = cis.read(buffer)) > 0) {
				fout.write(buffer, 0, len);
				x+=len;
			}
			
			dos.close();
			checksum = md.digest();
			 sb = new StringBuilder();
			    for (byte b : checksum) {
			        sb.append(String.format("%02x", b));
			    }
			    System.out.println("unencrypted: \n\tchecksum:\t" + sb.toString()+ " (" + md.getAlgorithm() +  ")"+ "\n\tFile size:\t" + x + " Byte");
		    
			fout.close();
			cis.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Datei nicht gefunden");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/* unwraps AES key with RSA private key */
	void unwrapKey() {
		KeyFactory keyFactory;
		try {
			keyFactory = KeyFactory.getInstance("RSA");

			PrivateKey pkey = keyFactory.generatePrivate(privateKey);

			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.UNWRAP_MODE, pkey);
			aesKey = cipher.unwrap(encryptedAESKey, "AES", Cipher.SECRET_KEY);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/* reads input file from SendSecureFile */ 
	void readInputFile(String file) {

		try {

			File f = new File(file);
			DataInputStream is = new DataInputStream(new FileInputStream(f));
		
			int len = is.readInt();
			encryptedAESKey = new byte[len];
			is.read(encryptedAESKey);
	
			len = is.readInt();
			signature = new byte[len];
			is.read(signature);
	
			byte buf[] = new byte[4096];
			ByteArrayOutputStream tmp = new ByteArrayOutputStream();
			while((len = is.read(buf)) > 0) {
				tmp.write(buf, 0, len);
			}
			
			encryptedFile = tmp.toByteArray();
			
			tmp.close();
			is.close();

		} catch (IOException e) {
			System.out.println(e);
		} 

	}
	/* reads keyfile from RSAKeyCreation */
	void readKeyFile(String path, boolean isPublic) {
		try {

			DataInputStream is = new DataInputStream(new FileInputStream(path));

			int len = is.readInt();

			byte[] name = new byte[len * 2];
			is.read(name);
			this.name = String.valueOf(name);
			len = is.readInt();

			byte[] key = new byte[len];

			is.read(key);
			is.close();

			if (isPublic) {

				publicKey = new X509EncodedKeySpec(key);

			} else {
				privateKey = new PKCS8EncodedKeySpec(key);

			}

		} catch (IOException e) {
			System.out.println(e);
		} 

	}
}
